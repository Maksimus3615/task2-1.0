package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class MultiTable {
    private static final String SYS_PARAM = "type";
    private static final String PATH = "table.properties";
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiTable.class);

    public static void main(String[] args) {
        try {
            Properties properties = getProperties(PATH);
            String propertyStr ="received setting data for the table:" + "\n" +
                    "inc= " + properties.getProperty("inc") + "  " +
                    "min= " + properties.getProperty("min") + "  " +
                    "max= " + properties.getProperty("max") + "  " +
                    "type=" + System.getProperty(SYS_PARAM);
            LOGGER.info(propertyStr);

            Table table = new Table(
                    properties.getProperty("min"),
                    properties.getProperty("max"),
                    properties.getProperty("inc"),
                    System.getProperty(SYS_PARAM));
            String tableStr = String.format("%n%s%n", table);
            LOGGER.info(tableStr);
        } catch (Exception e) {
            makeStackTraceMessage(e);
        }
    }

    public static Properties getProperties(String path) throws IOException {
        Properties properties = new Properties();
         InputStream in = MultiTable.class.getClassLoader().getResourceAsStream(path);
        if (in != null) {
            InputStreamReader inReader = new InputStreamReader(in, StandardCharsets.UTF_8);
            BufferedReader buff = new BufferedReader(inReader);
            properties.load(buff);
        } else {
            throw new FileNotFoundException("Error: file properties was not found...");
        }
        return properties;
    }

    private static void makeStackTraceMessage(Exception e) {
        LOGGER.error(e.getMessage());
        StackTraceElement[] stack = e.getStackTrace();
        StringBuilder exception = new StringBuilder();
        for (StackTraceElement s : stack) {
            exception.append(s.toString()).append("\n\t\t");
        }
        String message = String.format("%n%s", exception);
        LOGGER.error(message);
    }
}
