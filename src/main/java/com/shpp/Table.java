package com.shpp;

public class Table {
    private final Object[][] tab;
    private String message = "Calculation completed successfully.";
    private static final String WARNING = "Attention! " +
            "A data type overflow has occurred. Change the data type.";

    Table(String min, String max, String inc, String type) {
        double a = Double.parseDouble(min);
        double b = Double.parseDouble(max);
        double step = Double.parseDouble(inc);
        if (type == null) type = "int";

        int size = (int) ((b - a + 1) / step);
        tab = new Object[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                double result = (a + i * step) * (a + j * step);
                createTable(i, j, type, result);
            }
        }
    }

    private void createTable(int i, int j, String type, double result) {
        switch (type) {
            case "byte":
                tab[i][j] = (byte) (result);
                if (Math.abs(result) > Byte.MAX_VALUE)
                    message = WARNING;
                break;
            case "float":
                tab[i][j] = (float) (result);
                if (Math.abs(result) > Float.MAX_VALUE)
                    message = WARNING;
                break;
            case "double":
                tab[i][j] = result;
                break;
            case "short":
                tab[i][j] = (short) (result);
                if (Math.abs(result) > Short.MAX_VALUE)
                    message = WARNING;
                break;
            case "long":
                tab[i][j] = (long) (result);
                if (Math.abs(result) > Long.MAX_VALUE)
                    message = WARNING;
                break;
            default:
                tab[i][j] = (int) (result);
                if (Math.abs(result) > Integer.MAX_VALUE)
                    message = WARNING;
        }
    }

    @Override
    public String toString() {
        String out;
        if (message.equals(WARNING))
            out = message;
        else {
            out = getTable();
        }
        return out;
    }

    private String getTable() {
        String out = "";
        int maxNbrDigits = 0;
        for (Object[] value : tab) {
            for (int j = 0; j < tab.length; j++)
                if (value[j].toString().length() > maxNbrDigits)
                    maxNbrDigits = value[j].toString().length();
        }
        int indent = 3;
        for (Object[] objects : tab) {
            for (int j = 0; j < tab.length; j++)
                out = String.format("%s%s", out,
                        String.format(
                                String.format("%%%ds", maxNbrDigits + indent),
                                objects[j]));
            out = String.format("%s%s", out, "\n");
        }
        out = out + message;
        return out;
    }
}
