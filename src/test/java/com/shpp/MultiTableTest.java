package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class MultiTableTest {

    private static final Logger logger = LoggerFactory.getLogger(MultiTableTest.class);
    private static final String PATH = "test.properties";

    @Test
    void getPropertiesMin() throws Exception {
        Properties properties = MultiTable.getProperties(PATH);
        assertEquals("100", properties.getProperty("min"));
        logger.info("\n" +"Test getPropertiesMin() passed successfully" + "\n");
    }

    @Test
    void getPropertiesMax() throws Exception {
        Properties properties = MultiTable.getProperties(PATH);
        assertEquals("110", properties.getProperty("max"));
        logger.info("\n" +"Test getPropertiesMax() passed successfully" + "\n");
    }

    @Test
    void getPropertiesInc() throws Exception {
        Properties properties = MultiTable.getProperties(PATH);
        assertEquals("1", properties.getProperty("inc"));
        logger.info("\n" +"Test getPropertiesInc() passed successfully" + "\n");
    }

    @Test
    void getPropertiesExc() {
        String noPath = "sss.properties";
        String exceptionMessage = "";
        try {
            MultiTable.getProperties(noPath);
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
        } finally {
            assertEquals("Error: " +
                    "file properties was not found...", exceptionMessage);
            logger.info("\n" +"Test getPropertiesExc() passed successfully" + "\n");
        }
    }
}
