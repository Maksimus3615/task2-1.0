package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableTest {
    private static final Logger logger = LoggerFactory.getLogger(TableTest.class);
    private static final String WARNING = "Attention! " +
            "A data type overflow has occurred. Change the data type.";
    private static final String a = "1";
    private static final String b = "10";
    private static final String step = "1";
    private static final String RESULT_INT =
            "     1     2     3     4     5     6     7     8     9    10\n" +
                    "     2     4     6     8    10    12    14    16    18    20\n" +
                    "     3     6     9    12    15    18    21    24    27    30\n" +
                    "     4     8    12    16    20    24    28    32    36    40\n" +
                    "     5    10    15    20    25    30    35    40    45    50\n" +
                    "     6    12    18    24    30    36    42    48    54    60\n" +
                    "     7    14    21    28    35    42    49    56    63    70\n" +
                    "     8    16    24    32    40    48    56    64    72    80\n" +
                    "     9    18    27    36    45    54    63    72    81    90\n" +
                    "    10    20    30    40    50    60    70    80    90   100\n" +
                    "Calculation completed successfully.";
    private static final String RESULT_DOUBLE =
            "     1.0     2.0     3.0     4.0     5.0     6.0     7.0     8.0     9.0    10.0\n" +
                    "     2.0     4.0     6.0     8.0    10.0    12.0    14.0    16.0    18.0    20.0\n" +
                    "     3.0     6.0     9.0    12.0    15.0    18.0    21.0    24.0    27.0    30.0\n" +
                    "     4.0     8.0    12.0    16.0    20.0    24.0    28.0    32.0    36.0    40.0\n" +
                    "     5.0    10.0    15.0    20.0    25.0    30.0    35.0    40.0    45.0    50.0\n" +
                    "     6.0    12.0    18.0    24.0    30.0    36.0    42.0    48.0    54.0    60.0\n" +
                    "     7.0    14.0    21.0    28.0    35.0    42.0    49.0    56.0    63.0    70.0\n" +
                    "     8.0    16.0    24.0    32.0    40.0    48.0    56.0    64.0    72.0    80.0\n" +
                    "     9.0    18.0    27.0    36.0    45.0    54.0    63.0    72.0    81.0    90.0\n" +
                    "    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0\n" +
                    "Calculation completed successfully.";

    @Test
    void byteTypeInDiapason() {

        String type = "byte";
        String result = new Table(a, b, step, type).toString();

        assertEquals(RESULT_INT, result);
        logger.info("\n" + "Test byteTypeInDiapason() passed successfully" + "\n");
    }

    @Test
    void byteTypeOutDiapason() {

        String type = "byte";
        String a1 = "120";
        String b1 = "130";
        String result = new Table(a1, b1, step, type).toString();

        assertEquals(WARNING, result);
        logger.info("\n" + "Test byteTypeOutDiapason() passed successfully" + "\n");
    }

    @Test
    void intTypeInDiapason() {
        String type = "int";
        String result = new Table(a, b, step, type).toString();

        assertEquals(RESULT_INT, result);
        logger.info("\n" + "Test intTypeInDiapason() passed successfully" + "\n");
    }

    @Test
    void intTypeOutDiapason() {
        String type = "int";
        String a1 = "2000000000";
        String b1 = "2000000010";
        String result = new Table(a1, b1, step, type).toString();

        assertEquals(WARNING, result);
        logger.info("\n" + "Test intTypeOutDiapason() passed successfully" + "\n");
    }

    @Test
    void floatTypeInDiapason() {
        String type = "float";
        String result = new Table(a, b, step, type).toString();

        assertEquals(RESULT_DOUBLE, result);
        logger.info("\n" + "Test floatTypeInDiapason() passed successfully" + "\n");
    }

    @Test
    void floatTypeOutDiapason() {
        String type = "float";
        String a1 = "3.4e+038";
        String b1 = 3.4e+038 + 10 + "";
        String result = new Table(a1, b1, step, type).toString();

        assertEquals(WARNING, result);
        logger.info("\n" + "Test floatTypeOutDiapason() passed successfully" + "\n");
    }

    @Test
    void doubleTypeInDiapason() {
        String type = "double";
        String result = new Table(a, b, step, type).toString();

        assertEquals(RESULT_DOUBLE, result);
        logger.info("\n" + "Test doubleTypeInDiapason() passed successfully" + "\n");
    }

    @Test
    void shortTypeInDiapason() {

        String type = "short";
        String result = new Table(a, b, step, type).toString();

        assertEquals(RESULT_INT, result);
        logger.info("\n" + "Test shortTypeInDiapason() passed successfully" + "\n");
    }

    @Test
    void shortTypeOutDiapason() {

        String type = "short";
        String a1 = "30000";
        String b1 = "30010";
        String result = new Table(a1, b1, step, type).toString();

        assertEquals(WARNING, result);
        logger.info("\n" + "Test shortTypeOutDiapason() passed successfully" + "\n");
    }

    @Test
    void longTypeInDiapason() {
        String type = "long";
        String result = new Table(a, b, step, type).toString();

        assertEquals(RESULT_INT, result);
        logger.info("\n" + "Test longTypeInDiapason() passed successfully" + "\n");
    }

    @Test
    void longTypeOutDiapason() {
        String type = "long";
        String a1 = 9e+18 + "";
        String b1 = 9e+18 + 10 + "";
        String result = new Table(a1, b1, step, type).toString();

        assertEquals(WARNING, result);
        logger.info("\n" + "Test longTypeOutDiapason() passed successfully" + "\n");
    }
}